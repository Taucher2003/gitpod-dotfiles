#!/usr/bin/env bash

sudo apt install openssl -y
mkdir -p ~/.ssh
~/.dotfiles/decrypt_key.sh
ln -s ~/.dotfiles/git_ssh_signing_key.pub ~/.ssh/git_ssh_signing_key.pub
git config --global gpg.format ssh
git config --global user.signingkey ~/.ssh/git_ssh_signing_key.pub
git config --global commit.gpgsign true
git config --global gpg.ssh.allowedSignersFile "~/.dotfiles/allowed_ssh_git_signers"
chmod 600 ~/.ssh/git_ssh_signing_key
ln -s ~/.dotfiles/.bash_aliases ~/.bash_aliases
