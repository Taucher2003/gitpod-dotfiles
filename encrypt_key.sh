openssl enc -aes-256-cbc -pbkdf2 -iter 20000 -in git_ssh_signing_key -out git_ssh_signing_key.encrypted -pass "pass:$GIT_SSH_SIGNING_KEY_ENCRYPTION_PASSWORD"
