eval `ssh-agent -s`

signing_key() {
    ssh-add ~/.ssh/git_ssh_signing_key
}

install_firefox() {
    sudo add-apt-repository ppa:mozillateam/ppa
    echo '
Package: *
Pin: release o=LP-PPA-mozillateam
Pin-Priority: 1001

Package: firefox
Pin: version 1:1snap*
Pin-Priority: -1
' | sudo tee /etc/apt/preferences.d/mozilla-firefox
    sudo apt install firefox
}
